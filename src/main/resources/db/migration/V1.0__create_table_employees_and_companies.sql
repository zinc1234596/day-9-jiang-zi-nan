CREATE TABLE companies
(
    ID         BIGINT NOT NULL AUTO_INCREMENT,
    name       VARCHAR(255),
    PRIMARY KEY (id)
);

CREATE TABLE employees
(
    ID         BIGINT NOT NULL AUTO_INCREMENT,
    name       VARCHAR(255),
    age        INTEGER,
    gender     VARCHAR(255),
    salary     FLOAT(53),
    company_id BIGINT,
    PRIMARY KEY (id),
    FOREIGN KEY (company_id) REFERENCES companies(ID)
);