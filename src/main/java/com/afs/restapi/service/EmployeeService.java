package com.afs.restapi.service;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final EmployeeJPARepository employeeJPARepository;

    public EmployeeService(InMemoryEmployeeRepository inMemoryEmployeeRepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.employeeJPARepository = employeeJPARepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return EmployeeMapper.toResponseList(employeeJPARepository.findAll());
    }

    public EmployeeResponse findById(Long id) {
        return EmployeeMapper.toResponse(employeeJPARepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new));
    }

    public EmployeeResponse update(Long id, Employee employee) {
        Employee toBeUpdatedEmployee = employeeJPARepository.findById(id)
                .orElseThrow(EmployeeNotFoundException::new);
        if (employee.getSalary() != null) {
            toBeUpdatedEmployee.setSalary(employee.getSalary());
        }
        if (employee.getAge() != null) {
            toBeUpdatedEmployee.setAge(employee.getAge());
        }
        return EmployeeMapper.toResponse(employeeJPARepository.save(toBeUpdatedEmployee));
    }

    public List<EmployeeResponse> findAllByGender(String gender) {
        return EmployeeMapper.toResponseList(employeeJPARepository.findByGender(gender));
    }

    public EmployeeResponse create(Employee employee) {
        return EmployeeMapper.toResponse(employeeJPARepository.save(employee));
    }

    public List<EmployeeResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return EmployeeMapper.toResponseList(employeeJPARepository.findAll(pageRequest).getContent());
    }

    public void delete(Long id) {
        employeeJPARepository.deleteById(id);
    }
}
