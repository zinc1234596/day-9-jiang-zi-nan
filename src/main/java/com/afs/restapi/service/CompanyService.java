package com.afs.restapi.service;

import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.repository.CompanyJPARepository;
import com.afs.restapi.repository.EmployeeJPARepository;
import com.afs.restapi.repository.InMemoryCompanyRepository;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.repository.InMemoryEmployeeRepository;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeResponse;
import com.afs.restapi.service.mapper.CompanyMapper;
import com.afs.restapi.service.mapper.EmployeeMapper;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CompanyService {

    private final InMemoryCompanyRepository inMemoryCompanyRepository;
    private final InMemoryEmployeeRepository inMemoryEmployeeRepository;

    private final CompanyJPARepository companyJPARepository;

    public CompanyService(InMemoryCompanyRepository inMemoryCompanyRepository, InMemoryEmployeeRepository inMemoryEmployeeRepository, CompanyJPARepository companyJPARepository, EmployeeJPARepository employeeJPARepository) {
        this.inMemoryCompanyRepository = inMemoryCompanyRepository;
        this.inMemoryEmployeeRepository = inMemoryEmployeeRepository;
        this.companyJPARepository = companyJPARepository;
    }

    public InMemoryCompanyRepository getCompanyRepository() {
        return inMemoryCompanyRepository;
    }

    public InMemoryEmployeeRepository getEmployeeRepository() {
        return inMemoryEmployeeRepository;
    }

    public List<CompanyResponse> findAll() {
        return CompanyMapper.toResponseList(companyJPARepository.findAll());
    }

    public List<CompanyResponse> findByPage(Integer page, Integer size) {
        PageRequest pageRequest = PageRequest.of(page - 1, size);
        return CompanyMapper.toResponseList(companyJPARepository.findAll(pageRequest).getContent());
    }

    public CompanyResponse findById(Long id) {
        return CompanyMapper.toResponse(companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new));
    }

    public CompanyResponse update(Long id, Company company) {
        Company findedCompany = companyJPARepository.findById(id).orElseThrow(CompanyNotFoundException::new);
        findedCompany.setName(company.getName());
        return CompanyMapper.toResponse(companyJPARepository.save(findedCompany));
    }

    public CompanyResponse create(Company company) {
        return CompanyMapper.toResponse(companyJPARepository.save(company));
    }

    public List<EmployeeResponse> findEmployeesByCompanyId(Long id) {
        Company company = companyJPARepository.findById(id)
                .orElseThrow(CompanyNotFoundException::new);
        return EmployeeMapper.toResponseList(company.getEmployees());
    }

    public void delete(Long id) {
        companyJPARepository.deleteById(id);
    }
}
