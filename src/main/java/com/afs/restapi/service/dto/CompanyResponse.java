package com.afs.restapi.service.dto;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;

import java.util.List;

public class CompanyResponse {

    private Long id;
    private String name;

    private Integer employeeCount;

    public CompanyResponse(Company company) {
        employeeCount = this.countEmployee(company);
    }

    public Integer countEmployee(Company company){
        return company.getEmployees().size();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getEmployeeCount() {
        return employeeCount;
    }

    public void setEmployeeCount(Integer employeeCount) {
        this.employeeCount = employeeCount;
    }
}
