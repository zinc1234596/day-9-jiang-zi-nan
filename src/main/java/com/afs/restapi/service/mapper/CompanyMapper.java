package com.afs.restapi.service.mapper;

import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.CompanyRequest;
import com.afs.restapi.service.dto.CompanyResponse;
import com.afs.restapi.service.dto.EmployeeRequest;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyMapper {
    private CompanyMapper(){

    }
    public static Company toEntity(CompanyRequest request) {
        Company company = new Company();
        BeanUtils.copyProperties(request, company);
        return company;
    }

    public static CompanyResponse toResponse(Company company){
        CompanyResponse companyResponse = new CompanyResponse(company);
        BeanUtils.copyProperties(company, companyResponse);
        return companyResponse;
    }

    public static List<CompanyResponse> toResponseList(List<Company> companyList){
        return companyList.stream()
                .map(CompanyMapper::toResponse)
                .collect(Collectors.toList());
    }
}
