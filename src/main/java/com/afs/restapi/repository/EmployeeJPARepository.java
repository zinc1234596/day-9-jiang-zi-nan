package com.afs.restapi.repository;

import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.dto.EmployeeRequest;
import com.afs.restapi.service.dto.EmployeeResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EmployeeJPARepository extends JpaRepository<Employee,Long> {
//    @Query("SELECT new com.afs.restapi.service.dto.EmployeeResponse(e.id, e.name, e.age, e.gender) FROM Employee e")
//    List<EmployeeResponse> findAllPublicEmployees();
//
//    @Query("SELECT new com.afs.restapi.service.dto.EmployeeResponse(e.id, e.name, e.age, e.gender) FROM Employee e WHERE e.id = :id")
//    EmployeeResponse findEmployeeById(@Param("id") Long id);

//    @Query("SELECT new com.afs.restapi.service.dto.EmployeeResponse(e.id, e.name, e.age, e.gender) FROM Employee e WHERE e.gender = :gender")
//    List<EmployeeResponse> findByGender(@Param("gender") String gender);
List<Employee> findByGender(String gender);
}
